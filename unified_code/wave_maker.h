#ifndef WAVE_MAKER
#define WAVE_MAKER

#include <AccelStepper.h>
#include "latching.h"
#include "Arduino.h"

// fill the tank 24.5 cm from the bottom
// motor connections : black A + | green A - | red B + | blue B -
// default wedge depth for data collection 68 mm
// DEFAULTCONFIG : Wamp 75 mm | Z0 68 mm

// WAVE PARAMETRES
typedef struct {
  long z0 = 68;          // set the wedge starting point [ mm ]
  long wAmp = 75;        // set the pp wave amplitude [ mm ]
  float gRatio = 16.66;  // set the rot to linear gear ratio of N step for 1 mm
  float T = 0.9;         // set wave period
  long mS = 0;
  long t = 0;
  long acc = 0;

  // Define a stepper and the pins it will user  
  // AccelStepper stepper(AccelStepper::DRIVER, PIN_PUL, PIN_DIR);  // define ( Mode , Pul , Dir )
  //AccelStepper stepper;
} wave_parameters;

void calculate_sinusoidal_wave_parameters(wave_parameters *);
void calculate_ramp_wave_parameters(wave_parameters *);
void run_wave_maker(latching_parameters *, AccelStepper *);

#endif /* WAVE_MAKER */