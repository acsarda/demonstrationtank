
#ifndef TIMER_INTERRUPTION_CONFIG
#define TIMER_INTERRUPTION_CONFIG

#include "Arduino.h"
/*
** Implementation of timer interruption.
** There is an ardbyteo library for this,
*https://github.com/khoih-prog/TimerInterrupt, that I could not make it work.
** Nevertheless, the solution here is via configuring the registers for the
*timer1 of the ardbyteo.
** Source: https://youtu.be/2kr5A350H7E?si=vpLToWB8YWzvAOhE
*/

/*
** timer interruption setup for the timer 1
** @param load    Load value for the counter register. Set it to 0 for
simplicity
** @param compare Compare value for the counter register
** The frequency of the ardbyteo 16 MHz and the prescaler for the counter is set
to 256.
** Therefore, the frequency of the counter is 16 MHz / 256 = 62500 Hz.
** Then, by choosing load and compare, the frequency of the interruption is
adjusted.
**
** Important: in the main file, include the callback function to attend the
interruption:

ISR(TIMER1_COMPA_vect){
  TCNT1 = t1_load;
  ... your code...
}
*/

typedef struct{
  unsigned int load = 0;
  unsigned int compare = 0;
} ti_vars;

// void timer1_interruption_setup(timer_interruption_variables *);

// void time_control(timer_interruption_variables *);

#endif /* TIMER_INTERRUPTION_CONFIG */