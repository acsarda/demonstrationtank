#ifndef LATCHING
#define LATCHING

#include "Arduino.h"

typedef enum{
  HOLD,
  RELEASED,
} buoy_status;

//int latching = 0;

typedef struct {
  unsigned long time;
  unsigned long time_init_latching;
  unsigned long time_latching;
  unsigned long time0;
  unsigned long dy;
  unsigned long time_pote0 = millis();
  byte start_latching_cycle;
  byte allow_latching ;
  byte allow_hold;
  byte update_latching_time;
  buoy_status status;
} latching_parameters;

void hold_buoy(latching_parameters *);
void release_buoy(latching_parameters *);
void run_latching(latching_parameters *);

#endif /* LATCHING */