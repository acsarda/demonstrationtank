#include "Arduino.h"
#include "wave_maker.h"
#include "latching.h"
#include <AccelStepper.h>

void calculate_sinusoidal_wave_parameters(wave_parameters *wp){
  wp->mS =(long) 30000 * wp->gRatio;
  wp->acc = (long)(wp->wAmp) / ((wp->T * wp->T) / 256);
  wp->wAmp = (long) (wp->wAmp / 2) * wp->gRatio;
}

void calculate_ramp_wave_parameters(wave_parameters *wp){
  wp->mS = (long)((wp->wAmp * 2) / wp->T) * wp->gRatio;
  wp->acc = 80000;
}

void run_wave_maker(latching_parameters *lp, AccelStepper *stepper){
  // If at the end of travel go to the other end
  if ((stepper->distanceToGo()) == 0){
  // period calculation
  // Serial . println ( millis () -t ) ;
  // t = millis () ;
  stepper->moveTo(-stepper->currentPosition());
  
  // LATCHING SIGNAL
  if((stepper->distanceToGo()) < 0){
    lp->allow_latching = 1;
  } 
  else {
    lp->allow_latching = 0;
    }
  }
  stepper->run();
}