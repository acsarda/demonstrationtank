// #include "Arduino.h"
#include <Bounce2.h>
#include <SoftwareSerial.h>

#include "timer_interruption_config.h"
#include "latching.h"
#include "wave_maker.h"
#include "pin_io.h"

// Define a stepper and the pins it will user 
AccelStepper stepper(AccelStepper::DRIVER, PIN_PUL, PIN_DIR);  // define ( Mode , Pul , Dir )

//Button buttonTop = Button();
Bounce2::Button buttonTop = Bounce2::Button();

latching_parameters lp;
wave_parameters wp;
// Timer interruption variables
// ti_vars tvars;
// I get a square signal of 50 Hz, i.e., an interrupt freq of 100 Hz (10ms). 
// The theorical value should be 625, but using the oscilloscope it works more precisely with 622

void setup(){
  
  Serial.begin(9600);

  /*
  ###### Wave maker setup ######
  */  

  // setting up the inizialization button
  buttonTop.attach(PIN_CALIBRATION, INPUT_PULLUP); 
  buttonTop.interval(5);
  buttonTop.setPressedState(LOW);

  // WAVE MAKER PARAMETERS CALCULATION
  wp.z0 = (long)wp.z0 * wp.gRatio;
  calculate_sinusoidal_wave_parameters(&wp);
  // STEPPER INIZIALIZATION
  stepper.setMaxSpeed(800);
  stepper.setAcceleration(wp.acc);
  delay(3000);

  // inizialization
  buttonTop.update();
  while(!buttonTop.pressed()){
    stepper.move(-1);
    stepper.run();
    buttonTop.update();
  }
  // Serial . println (" buttonTop ") ;
  delay(1000);
  stepper.move(wp.z0);
  stepper.runToPosition() ;
  delay(3000);
  
  // stepper configuration
  stepper.setCurrentPosition(0);
  stepper.setMaxSpeed(wp.mS) ;
  stepper.moveTo(wp.wAmp);

  /*
  ###### Latching setup ######
  */
  // entering in automatic latching mode
  Serial.println(" automatic mode ");
  // pinMode(link, INPUT_PULLUP);
  lp.dy = 200;

  pinMode(SOLENOID, OUTPUT);
  digitalWrite(SOLENOID, LOW);
  
  // t = 0;
  // Update = millis(); 

  /*
  ###### Timer interrupt ######
  */
  /*
  tvars.compare = 622;
  // reset timer1 control reg A
  TCCR1A = 0;

  // set the prescaler of 256
  TCCR1B |= (1 << CS12);
  TCCR1B &= ~(1 << CS11);
  TCCR1B &= ~(1 << CS10);

  // reset timer1 and set compare value
  TCNT1 = tvars.load;
  OCR1A = tvars.compare;

  // enable timer1 compare interrupt
  TIMSK1 = (1 << OCIE1A);
  //enable global interrupts
  sei();
  */
}

void loop(){
  run_wave_maker(&lp, &stepper);
  run_latching(&lp);
}
/*
ISR(TIMER1_COMPA_vect){
  TCNT1 = tvars.load;
  // PORTD ^= (1 << pulse_pin); 
  lp.update_latching_time = 1;
}
*/