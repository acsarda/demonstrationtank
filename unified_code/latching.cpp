#include "latching.h"
#include "pin_io.h"

#include "Arduino.h"

void hold_buoy(latching_parameters *lp){
  if(lp->status == RELEASED && lp->allow_hold){
    digitalWrite(SOLENOID, HIGH);
    lp->time_init_latching = millis();

    lp->allow_hold = 0;    
    lp->status = HOLD;
  }
}

void release_buoy(latching_parameters *lp){
  unsigned long time_elapsed = millis() - lp->time_init_latching;

  if((lp->status == HOLD) && (time_elapsed > lp->time_latching)){
    digitalWrite(SOLENOID, LOW);
    lp->status = RELEASED;
  }
}

void run_latching(latching_parameters *lp){
  // lp->allow_latching is set/unset by the wave maker
  if (lp->allow_latching) {
    lp->time0 = millis();
    lp->start_latching_cycle = 1;
    lp->allow_latching = 0;
  }
  
  // if condition inherited from Jacopo
  if (lp->start_latching_cycle && ((millis() - lp->time0) > lp->dy)) {
    lp->allow_hold = 1;
    lp->start_latching_cycle = 0;
  }

  hold_buoy(lp);
  release_buoy(lp);

  // update latching time from the potentiometer
  if(lp->update_latching_time){
    // From Jacopo: 
    // convert the potentiometer read to a [ ms ] value ( A0 goes from 0 to 1023) (choose the value of
    // upper scale as double the optimal [ ms ] latching time )
    lp->time_latching = (analogRead(POTENTIOMETER) * 1060) / 1023;        
  
    lp->update_latching_time = 0;
  }

  // time control to check the potentiometer
  if((millis() - lp->time_pote0) > 100){
    lp->update_latching_time = 1;
    lp->time_pote0 = millis();
  }
}