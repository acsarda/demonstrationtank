#ifndef PIN_IO
#define PIN_IO

// Wave maker
#define PIN_PUL 7
#define PIN_DIR 6
// #define PIN_LINK 5         // pin for the link. This one now is gone
#define PIN_CALIBRATION 4  // pin for the calibration. 
// Latching controller
#define SOLENOID 5
#define POTENTIOMETER A0

#endif /* PIN_IO */