# Demonstration Tank

## Introduction

The objective is to improve how the demonstration wave tank behaves

## To do list

- [x] Unify code in a single Arduino
- [ ] GUI for entering latching time
- [ ] Serial communication to enter to the Arduino the chosen latching time
- [ ] Wave up measurement
- [ ] Position and velocity measurement
- [ ] Wave and position displacement
- []
