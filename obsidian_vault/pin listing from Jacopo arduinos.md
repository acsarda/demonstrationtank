# Wave maker arduino
| Pin | Digital or analog | I/O | Description |
| ---- | ---- | ---- | ---- |
| 5 | D | O | Link for the latching.<br>I think the latching start working after the wave maker command it. Likely to be removed by software. |
| 4 | D | I | Calibration pin. It is used as a switch. It uses bounce2 library. |
| 6 | D | O | Direction input for the driver of the step motor. It is connected to DIR- in the driver. |
| 7 | D | O | Step input for the driver of the step motor. It is connected to PUL- in the driver. |
Total: 4 digital pins

# Latching

| Pin | Digital or analog | I/O | Description |
| ---- | ---- | ---- | ---- |
| 5 | D | I | Link with the wave maker arduino. |
| 4 | D | O | Trigger signal for the solenoid |
| A0 | A | I | Latching time regulation |
Total: 2 digital pins, 1 analog.
