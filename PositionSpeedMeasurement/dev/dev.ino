#include "timer_interruption_config.h"
#include <Wire.h>
#include "Adafruit_VL6180X.h"

#define TIMER1_FREQ 100

// timer interruption config variables
const uint16_t t1_load = 0;
const uint16_t t1_comp = 622; // I get a square signal of 50 Hz, i.e., an interrupt freq of 100 Hz (10ms). 
// The theorical value should be 625, but using the oscilloscope it works more precisely with 622

enum booleans {RESET = 0, SET};

struct{
  uint8_t freq100Hz = RESET;
} flags;

struct Optical_Sensor{
  uint8_t range_actual;
  uint8_t range_previous = 0;
  int16_t velocity;
  uint8_t status;
  Adafruit_VL6180X sensor = Adafruit_VL6180X();
} optical_sensor;

void optical_sensor_read(Optical_Sensor *);

void setup(){
  // Timer interruption setup
  timer1_interruption_setup(t1_load, t1_comp);
  //enable global interrupts
  sei();
  
  /******************************************************/

  // Serial comm for the optical sensor
    Serial.begin(115200);

  // wait for serial port to open on native usb devices
  while (!Serial) {
    delay(1);
  }
  
  Serial.println("Adafruit VL6180x test!");
  if (! optical_sensor.sensor.begin()) {
    Serial.println("Failed to find sensor");
    while (1);
  }
  Serial.println("Sensor found!");

  /******************************************************/


  
}

void loop(){
  if(flags.freq100Hz){
    flags.freq100Hz = RESET;

    optical_sensor_read(&optical_sensor);
    if(optical_sensor.status == VL6180X_ERROR_NONE){
      //Serial.print("Displacement [mm]: "); 
      Serial.print(optical_sensor.range_actual);
      //Serial.print(" Velocity [mm/s]: "); 
      Serial.println(optical_sensor.velocity);
    }
  }
}

ISR(TIMER1_COMPA_vect){
  TCNT1 = t1_load;
  flags.freq100Hz = SET;
}

void optical_sensor_read(Optical_Sensor *os){
  int16_t difference;
  uint8_t range = os->sensor.readRange();;
  os->status = os->sensor.readRangeStatus();
  if(os->status == VL6180X_ERROR_NONE){
    difference = (int16_t)range - (int16_t)os->range_previous;
    if(abs(difference) > 2){
      os->velocity = difference * TIMER1_FREQ;
      os->range_previous = os->range_actual;
      os->range_actual = range;
    }
    else{
      os->velocity = 0;
      os->range_actual = os->range_previous;
    }
  }
}
