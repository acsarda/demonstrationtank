/*
** Implementation of timer interruption. 
** There is an arduino library for this, https://github.com/khoih-prog/TimerInterrupt, that I could not make it work.
** Nevertheless, the solution here is via configuring the registers for the timer1 of the arduino.
** Source: https://youtu.be/2kr5A350H7E?si=vpLToWB8YWzvAOhE
*/

/*
** timer interruption setup for the timer 1
** @param load    Load value for the counter register. Set it to 0 for simplicity
** @param compare Compare value for the counter register
** The frequency of the arduino 16 MHz and the prescaler for the counter is set to 256.
** Therefore, the frequency of the counter is 16 MHz / 256 = 62500 Hz.
** Then, by choosing load and compare, the frequency of the interruption is adjusted.
**
** Important: in the main file, include the callback function to attend the interruption:

ISR(TIMER1_COMPA_vect){
  TCNT1 = t1_load;
  ... your code...
}

*/
void timer1_interruption_setup(const uint16_t load, const uint16_t compare){

  // reset timer1 control reg A
  TCCR1A = 0;

  // set the prescaler of 256
  TCCR1B |= (1 << CS12);
  TCCR1B &= ~(1 << CS11);
  TCCR1B &= ~(1 << CS10);

  // reset timer1 and set compare value
  TCNT1 = load;
  OCR1A = compare;
  
  // enable timer1 compare interrupt
  TIMSK1 = (1 << OCIE1A);
}