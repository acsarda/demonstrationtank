#include <EEPROM.h>

#include <Wire.h>
#include "Adafruit_VL6180X.h"
#include <CapacitiveSensor.h>
#include <Bounce2.h>

//sensor object creation
Adafruit_VL6180X vl = Adafruit_VL6180X();
// CapacitiveSensor   cs_4_2 = CapacitiveSensor(10,11);
//button creation (for calibration)
Bounce2::Button cal =Bounce2::Button();

uint8_t range=0;
long wave=0;
float waveF=0;
long val=0;
long t=0;
int i=0;

long z0=0;
long z2=0;
long z_2=0;
float m2=0;
float m_2=0;

long ts= 0;
uint8_t range_1=0;
double s= 0.0000;

void setup() {
  // cs_4_2.set_CS_AutocaL_Millis(0xFFFFFFFF);
  Serial.begin(9600);

  // wait for serial port to open on native usb devices
  while (!Serial) {
    delay(1);
  }
  
  //check for TOF (time of flight) sensor
  Serial.println("Adafruit VL6180x test!");
  if (! vl.begin()) {
    Serial.println("Failed to find sensor");
    while (1);
  }
  Serial.println("Sensor found!");
  //setting up the button
  cal.attach(7, INPUT_PULLUP ); //pin 4 for the switch
  cal.interval(5); 
  cal.setPressedState(LOW);

  pinMode(LED_BUILTIN,OUTPUT);
  digitalWrite(LED_BUILTIN,LOW);
  delay(500);

  t=millis();
  //waiting for pressing calibration button --> go into calibrtion mode
  while(millis()-t<=4000 && !cal.pressed()){
    cal.update();
    Serial.println("wait");
    delay(10);
  }
  
  //entering in calibration mode if button is pressed
  if(cal.pressed()){
  digitalWrite(LED_BUILTIN,HIGH);
  cal.update();
  while(!cal.pressed()){
    // Serial.println(cs_4_2.capacitiveSensorRaw(50));
    cal.update();
    delay(1);
  }
  //reading value z0 and store it in the EPROM (arduino permanent memory)
  for(int i=0;i<1000;i++){
    // val=val+cs_4_2.capacitiveSensorRaw(50);
    delay(3);
  }
  z0 = (long) val/1000;
  //storing value in EPROM
  for(i=0;i<=4;i++){
  val=(long) z0/10;
  EEPROM.write(i,z0-(val*10)); 
  z0=val;
  }
  val=0;
  delay(10);
  
  cal.update();
  while(!cal.pressed()){
    Serial.println(cs_4_2.capacitiveSensorRaw(50));
    cal.update();
    delay(1);
  }
  //reading value z2 (+2cm) and store it in the EPROM (arduino permanent memory)
  for(int i=0;i<1000;i++){
    val=val+cs_4_2.capacitiveSensorRaw(50);
    delay(3);
  }
  z2 = (long) val/1000;
  //storing value in EPROM
  for(i=5;i<=9;i++){
  val=(long) z2/10;
  EEPROM.write(i,z2-(val*10)); 
  z2=val;
  }
  val=0;
  delay(10);
  
  cal.update();
  while(!cal.pressed()){
    Serial.println(cs_4_2.capacitiveSensorRaw(50));
    cal.update();
    delay(1);
  }
  //reading value z_2 (-2cm) and store it in the EPROM (arduino permanent memory)
  for(int i=0;i<1000;i++){
    val=val+cs_4_2.capacitiveSensorRaw(50);
    delay(3);
  }
  z_2 = (long) val/1000;
  //storing value in the EPROM
  for(i=10;i<=14;i++){
  val=(long) z_2/10;
  EEPROM.write(i,z_2-(val*10)); 
  z_2=val;
  }
  val=0;
  delay(10);
  }
  //reading calibration values (z0 z2 z_2) from EPROM
    for(i=4;i>=1;i--){
    z0=(z0+(long)EEPROM.read(i))*10;
    }
    z0=z0+(long)EEPROM.read(0);
    for(i=9;i>=6;i--){
    z2=(z2+(long)EEPROM.read(i))*10;
    }
    z2=z2+(long)EEPROM.read(5);
    for(i=14;i>=11;i--){
    z_2=(z_2+(long)EEPROM.read(i))*10;
    }
    z_2=z_2+(long)EEPROM.read(10);
   
    Serial.println("valori");
    Serial.println(z0);
    Serial.println(z2);
    Serial.println(z_2);
  
  digitalWrite(LED_BUILTIN,LOW);
  
  //doing linear interpolation for relating capacitance data and mm
  m2=2.0/(z0-z2);
  m_2=-2.0/(z0-z_2);
  delay(1000);
  ts=millis();
}

void loop() {
  
  //read buoy position
  range = vl.readRange();
  uint8_t status = vl.readRangeStatus();
  //reading wave water surface
  // long wave =  cs_4_2.capacitiveSensorRaw(50);
  if(wave>=z0){
    waveF =-(wave-z0)*m2*10;
    }else{
    waveF =-(wave-z0)*m_2*10;  
  }
  //calculating buoy speed
  s=((range-range_1))/((millis()-ts));
  ts=millis();
  range_1=range;

  //printing variables for Serial Plot format ("name of variable:" "value" ",") (the last one Println without comma)
  if (status == VL6180X_ERROR_NONE) {
     Serial.print("Range:");Serial.print(range-26);Serial.print(",");
     //Serial.print("Speed:");Serial.print(s);Serial.print(",");
     Serial.print("Wave:");Serial.println(waveF);
  }

  delay(10);
}