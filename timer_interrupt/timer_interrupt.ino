#include "timer_interruption_config.h"

const uint8_t pulse_pin = PD4;

// timer interruption config variables
const uint16_t t1_load = 0;
const uint16_t t1_comp = 622; // I get a square signal of 50 Hz, i.e., an interrupt freq of 100 Hz (10ms). 
// The theorical value should be 625, but using the oscilloscope it works more precisely with 622

void setup(){
  // set pin as output
  DDRB |= (1 << pulse_pin);

  timer1_interruption_setup(t1_load, t1_comp);
  //enable global interrupts
  sei();
}

void loop(){
  //delay(500);
}

ISR(TIMER1_COMPA_vect){
  TCNT1 = t1_load;
  PORTD ^= (1 << pulse_pin); 
}
